#!/bin/bash

if [ $# -gt 0 ]; then
	ansible-playbook -i my_inventory.yml --tags="$1" playbook.yml
else
	cat <<-EOS
	Call $0 with one of these arguments. For example, '$0 hello'.
	- ping: Simple connection test.
	- hello: Standard message.
	- ensure_ssh_config: Create /tmp/config.
EOS
fi
